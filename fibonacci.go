package fibonacci

import (
	"strconv"
)

// Fibonacci function
func Fibonacci(n int) string {
	var nextTerm int
	var result string
	t1 := 0
	t2 := 1

	for i := 0; i <= n; i++ {
		result = result + strconv.Itoa(t1) + ", "
		nextTerm = t1 + t2
		t1 = t2
		t2 = nextTerm
	}

	return result
}
