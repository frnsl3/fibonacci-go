package fibonacci

import (
	"regexp"
	"testing"
)

func TestFibonacciPrint(t *testing.T) {
	name := "0, 1, 1, 2,"
	want := regexp.MustCompile(name)
	msg := Fibonacci(3)
	if !want.MatchString(msg) {
		t.Fatalf(`Fibonacci(3) = %q, want match for %#q, nil`, msg, want)
	}
}

func BenchmarkFibonacciPrint(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		Fibonacci(3)
	}
}
